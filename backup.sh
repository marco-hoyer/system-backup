#!/bin/bash
set -euo pipefail

report_status() {
  /opt/system-backup/report_result.sh $?
}

trap report_status EXIT

# source config for backup
source /etc/remote_backup_config.sh

# remove old backups
duplicity remove-all-but-n-full 2 --force "ftp://$FTP_USER@$FTP_SERVER/$HOSTNAME"

# create new incremental backup or fill backup if the last full backup is older than one month
duplicity --full-if-older-than 7D $ADDITIONAL_OPTS --exclude "/var/backups/remote" --exclude "/tmp" --exclude "/sys" --exclude "/proc" --exclude "/var/log/" --exclude "/var/lib/mysql/" / ftp://$FTP_USER@$FTP_SERVER/$HOSTNAME

# remove old backups
duplicity remove-all-but-n-full 2 --force "ftp://$FTP_USER@$FTP_SERVER/$HOSTNAME"
