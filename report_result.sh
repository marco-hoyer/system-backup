#!/usr/bin/env bash

source /etc/remote_backup_config.sh

URL="https://graphite-prod-01-eu-west-0.grafana.net/graphite/metrics"
METRIC_NAME="${HOSTNAME}.backup.status"

curl -X POST -H "Authorization: Bearer $GRAFANA_CLOUD_API_KEY" -H "Content-Type: application/json" "$URL" -d "[{
    \"name\": \"$METRIC_NAME\",
    \"interval\": 3600,
    \"value\": $1,
    \"time\": $(date +%s)
}]"
