#!/bin/bash
set -euo pipefail

# source config for backup
source /etc/remote_backup_config.sh

duplicity list-current-files "ftp://$FTP_USER@$FTP_SERVER/$HOSTNAME"
