#!/bin/bash
set -euo pipefail

# source config for backup
source /etc/remote_backup_config.sh

# remove old backups
duplicity remove-all-but-n-full 1 --force ftp://$FTP_USER@$FTP_SERVER/$HOSTNAME
