#!/usr/bin/env bash
set -euo pipefail

cp -n backup.cron /etc/cron.d/backup
cp -n remote_backup_config.sh /etc

apt install duplicity lftp ncftp -y
