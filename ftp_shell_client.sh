#!/bin/bash
set -euo pipefail

# source config for backup
source /etc/remote_backup_config.sh

echo "### Use password: $FTP_PASSWORD ###"
ncftp "ftp://$FTP_USER@$FTP_SERVER"
