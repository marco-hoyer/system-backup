#!/bin/bash
set -euo pipefail

if [ $# -ne 3 ]; then
        echo "Error, got $# parameters, need 3!"
        echo ""
        echo "USAGE: $0 <file/folder to restore> <time> <destination>"
        echo "EXAMPLE: $0 var/www 3D /var/www"
        exit 1
fi

# source config for backup
source /etc/remote_backup_config.sh

duplicity restore --file-to-restore "$1" --time "$2" "ftp://$FTP_USER@$FTP_SERVER/system-backup" "$3"
