#!/bin/sh
set -euo pipefail

# params test
if [ $# -ne 1 ]
 then
        echo "This script restores a given mysql dump file"
        echo "USAGE: db_restore.sh <db-dump-file>"
        exit 1
 fi

CONF=/etc/mysql/debian.cnf
SYSNAME=$(hostname)
MODULE=DB-RESTORE

sudo mysql --defaults-file=$CONF < $1
if [ $? -ne 0 ]; then
        echo "$MODULE [ERROR] - could not restore db: $FILE on $SYSNAME"
        exit 1
fi

echo "$MODULE [OK] - db restore successful on $SYSNAME"
exit 0
