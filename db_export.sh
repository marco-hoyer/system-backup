#!/bin/bash
set -euo pipefail

# params test
if [ $# -ne 1 ] && [ $# -ne 2 ]
 then
        echo "This script exports all mysql db's except standard ones, or a given one, to a given directory"
        echo "USAGE: db_export.sh <target-dir> OPTIONAL: <db-name>"
        exit 1
 fi

 report_status() {
   /opt/system-backup/report_result.sh $?
 }

 trap report_status EXIT

# TARGET: temp directory for exported db's
# IGNORE: list of db's to be ignored
# CONF: mysql sysmaintainer config file
# SYSNAME: hostname of this system
# MODULE: name of this module
TARGET=$1
IGNORE="phpmyadmin|icinga|information_schema"
CONF=/etc/mysql/debian.cnf
SYSNAME=$(hostname)
MODULE=DBEXPORT

# test if mysql binary is availably
type mysql >/dev/null 2>/dev/null

# create directory if it doesn't exist
mkdir -p "$TARGET"

# check which db's to export
if [ $# -eq 2 ]
 then
        echo "$MODULE [INFO] - exporting db $2 on $SYSNAME"
        DBS=$2
 else
        echo "$MODULE [INFO] - exporting all db's on $SYSNAME"
        # parse available db's
        DBS="$(/usr/bin/mysql --defaults-file=$CONF -Bse 'show databases' | /bin/grep -Ev $IGNORE)"
 fi

# export to REPL_TEMP
for DB in $DBS; do
        /usr/bin/mysqldump --defaults-file=$CONF --allow-keywords --flush-logs --single-transaction --databases --add-drop-database "$DB" > "$TARGET/$DB.sql"
done

DB_LIST=$(echo "${DBS}" | tr " " "," | tr "\n" ".")
echo "Successfully exported ${DB_LIST} to disk"
